use serenity::framework::standard::CommandError;
use serenity::model::prelude::*;

pub fn get_role_id<'a, T>(roles: T, role_name: &str) -> Result<RoleId, CommandError>
where
    T: IntoIterator<Item = (&'a RoleId, &'a Role)>,
{
    Ok(*roles
        .into_iter()
        .find(|(_, role)| role.name.eq_ignore_ascii_case(role_name))
        .ok_or_else(|| CommandError::from("Could not find role"))?
        .0)
}
