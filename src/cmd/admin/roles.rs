use crate::config;
use crate::utils::get_role_id;

use serenity::framework::standard::{macros::command, Args, CommandError, CommandResult};
use serenity::model::prelude::*;
use serenity::prelude::*;

#[command]
#[description("Creates a role")]
#[min_args(1)]
#[required_permissions("MANAGE_ROLES")]
async fn mkrole(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let role_name = args.message();

    let guild_id = msg.guild_id.unwrap();

    guild_id
        .create_role(&ctx.http, |r| r.name(role_name))
        .await
        .map_err(|_| CommandError::from("Could not create role"))?;

    msg.channel_id
        .say(&ctx.http, "Role created".to_string())
        .await?;

    Ok(())
}
#[command]
#[description("Removes a role")]
#[min_args(1)]
#[required_permissions("MANAGE_ROLES")]
async fn rmrole(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let role_name = args.message();

    let guild_lock = &msg.guild(&ctx.cache).await.unwrap();
    let guild = &guild_lock.read().await;

    let role_id = get_role_id(&guild.roles, role_name)?;

    guild
        .delete_role(&ctx.http, role_id)
        .await
        .map_err(|_| CommandError::from("Could not remove role"))?;

    msg.channel_id
        .say(&ctx.http, "Role removed".to_string())
        .await?;

    Ok(())
}
#[command]
#[description("Assigns a role to a specified user")]
#[min_args(2)]
#[required_permissions("MANAGE_ROLES")]
async fn setrole(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let user_id = args
        .single::<UserId>()
        .map_err(|_| CommandError::from("Could not read user"))?;

    let role_name = args.rest();

    let guild_lock = &msg.guild(&ctx.cache).await.unwrap();
    let guild = &guild_lock.read().await;

    let role_id = get_role_id(&guild.roles, role_name)?;

    let mut member = guild.member(ctx, user_id).await?;

    if member
        .user
        .has_role(&ctx, msg.guild_id.unwrap(), role_id)
        .await
        .map_err(|_| CommandError::from("Could not read if user has role"))?
    {
        return Err(CommandError::from("User already has that role"));
    }

    member.add_role(&ctx.http, role_id).await?;

    msg.channel_id.say(&ctx.http, "Role assigned!").await?;

    Ok(())
}

#[command]
#[description("Removes a role from a specified user")]
#[min_args(2)]
#[required_permissions("MANAGE_ROLES")]
async fn unsetrole(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let user_id = args.single::<UserId>()?;

    let role_name = args.rest();

    let guild_lock = &msg.guild(&ctx.cache).await.unwrap();
    let guild = &guild_lock.read().await;

    let role_id = get_role_id(&guild.roles, role_name)?;

    let mut member = guild.member(ctx, user_id).await?;

    if !member
        .user
        .has_role(&ctx, msg.guild_id.unwrap(), role_id)
        .await
        .map_err(|_| CommandError::from("Could not read if user has role"))?
    {
        return Err("User does not have that role".into());
    }

    member.remove_role(&ctx.http, role_id).await?;

    msg.channel_id.say(&ctx.http, "Role removed!").await?;

    Ok(())
}

#[command]
#[description("Verifies user")]
#[min_args(1)]
#[required_permissions("MANAGE_ROLES")]
async fn verify(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let user_id = args.single::<UserId>()?;

    let guild_id = msg.guild_id.unwrap();

    let cfg = match config::CONFIG
        .guilds()
        .get(&guild_id)
        .and_then(|c| c.join_message_config())
    {
        Some(x) => x,
        None => return Ok(()),
    };

    let role_id = match cfg.role {
        Some(x) => x,
        None => return Ok(()),
    };
    let guild_lock = &msg.guild(&ctx.cache).await.unwrap();
    let guild = &guild_lock.read().await;

    let mut member = guild.member(ctx, user_id).await?;

    if member
        .user
        .has_role(&ctx, msg.guild_id.unwrap(), role_id)
        .await
        .map_err(|_| CommandError::from("Could not read if user has role"))?
    {
        return Err("User is already verified".into());
    }

    member.add_role(&ctx.http, role_id).await?;

    msg.channel_id.say(&ctx.http, "User verified!").await?;

    let message = match &cfg.post_verification_message {
        Some(x) => x,
        None => return Ok(()),
    };

    let channel = match cfg.post_verification_channel {
        Some(x) => x,
        None => return Ok(()),
    };

    channel
        .say(&ctx.http, message.replace("$USER", &user_id.mention()))
        .await?;

    Ok(())
}
