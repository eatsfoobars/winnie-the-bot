use serenity::framework::standard::{macros::command, Args, CommandResult};
use serenity::model::prelude::*;
use serenity::prelude::*;

#[command]
#[description("Changes channel name")]
#[min_args(1)]
#[required_permissions("MANAGE_CHANNELS")]
async fn channame(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let chan_name = args.message();
    let chan_id = msg.channel_id;

    let chan = chan_id.edit(&ctx.http, |c| c.name(chan_name)).await?;

    chan_id
        .say(&ctx.http, format!("Name changed to #{}", chan.name))
        .await?;

    Ok(())
}
