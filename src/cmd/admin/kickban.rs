use serenity::framework::standard::{macros::command, Args, CommandError, CommandResult};
use serenity::model::prelude::*;
use serenity::prelude::*;

#[command]
#[description("Bans user")]
#[min_args(1)]
#[required_permissions("BAN_MEMBERS")]
async fn ban(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let user_id = args
        .single::<UserId>()
        .map_err(|_| CommandError::from("Could not read user"))?;

    let guild_id = msg.guild_id.unwrap();

    let ban_result = if let Ok(dmd) = args.single::<u8>() {
        guild_id.ban(&ctx.http, user_id, &(dmd, args.rest())).await
    } else {
        guild_id.ban(&ctx.http, user_id, &args.rest()).await
    };
    ban_result.map_err(|_| CommandError::from("Could not ban user"))
}

#[command]
#[description("Kicks user")]
#[min_args(1)]
#[required_permissions("KICK_MEMBERS")]
async fn kick(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let user_id = args
        .single::<UserId>()
        .map_err(|_| CommandError::from("Could not read user"))?;

    msg.guild_id
        .unwrap()
        .kick(&ctx.http, user_id)
        .await
        .map_err(|_| CommandError::from("Could not kick user"))
}
