mod channame;
mod clear;
mod kickban;
mod roles;
mod sleep;

use serenity::framework::standard::macros::group;

use channame::*;
use clear::*;
use kickban::*;
use roles::*;
use sleep::*;

#[group]
#[description("Administrative commands")]
#[only_in("guilds")]
#[commands(
    channame, clear, ban, kick, mkrole, rmrole, setrole, unsetrole, verify, sleep
)]
struct Admin;
