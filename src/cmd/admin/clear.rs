use crate::get_messages::*;
use chrono::offset::Utc;
use chrono::Duration;
use futures::prelude::*;
use futures::stream::FuturesUnordered;
use lazy_static::lazy_static;
use log::debug;
use serenity::framework::standard::{macros::command, Args, CommandResult};
use serenity::model::prelude::*;
use serenity::prelude::*;

const DELETE_MESSAGE_LIMIT: u64 = 100;

fn within_limit(msg: &Message) -> bool {
    lazy_static! {
        static ref TIME_LIMIT: Duration = Duration::weeks(2);
    }
    Utc::now().signed_duration_since(msg.timestamp) < *TIME_LIMIT
}

#[command]
#[description("Clears channel messages")]
#[min_args(1)]
#[required_permissions("MANAGE_MESSAGES")]
async fn clear(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let mut messages = if args.message() == "inf" {
        get_messages(&ctx, msg.channel_id, MessageRange::AllBefore(msg.id)).await
    } else if let (Ok(first_id), Ok(second_id)) = (args.single::<u64>(), args.single::<u64>()) {
        let before = first_id.max(second_id);
        let after = first_id.min(second_id);
        let mut msgs = get_messages(
            &ctx,
            msg.channel_id,
            MessageRange::Between(after.into(), before.into()),
        )
        .await;

        if let Ok(before_message) = msg.channel_id.message(&ctx.http, before).await {
            msgs.push(before_message);
        }

        if let Ok(after_message) = msg.channel_id.message(&ctx.http, after).await {
            msgs.push(after_message);
        }

        msgs
    } else {
        args.restore();
        if let Ok(n) = args.parse::<u64>() {
            get_messages(&ctx, msg.channel_id, MessageRange::Before(msg.id, n)).await
        } else {
            return Ok(());
        }
    };

    messages.push(msg.clone());
    debug!("got {} messages", messages.len());

    let (bulk, non_bulk): (Vec<Message>, Vec<Message>) =
        messages.into_iter().partition(within_limit);

    debug!("bulk: {} non-bulk: {}", bulk.len(), non_bulk.len());

    {
        let futures = FuturesUnordered::new();

        for chunk in bulk.chunks(DELETE_MESSAGE_LIMIT as usize) {
            futures.push(msg.channel_id.delete_messages(&ctx.http, chunk));
        }

        futures.for_each_concurrent(None, |_| async {}).await;
    }

    {
        let futures = FuturesUnordered::new();

        for del in non_bulk {
            futures.push(msg.channel_id.delete_message(&ctx.http, del.id));
        }

        futures.for_each_concurrent(None, |_| async {}).await;
    }

    Ok(())
}
