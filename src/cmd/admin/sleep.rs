use serenity::framework::standard::{macros::command, Args, CommandResult};
use serenity::model::prelude::*;
use serenity::prelude::*;
use std::thread;
use std::time::Duration;

#[command]
#[description("Sleeps for the specified amount of seconds")]
#[min_args(1)]
#[owners_only]
async fn sleep(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    if let Ok(secs) = args.single::<u64>() {
        thread::sleep(Duration::from_secs(secs));
        msg.channel_id.say(&ctx.http, "Woo").await?;
    }

    Ok(())
}
