use serenity::framework::standard::{
    macros::{command, group},
    CommandResult,
};
use serenity::model::prelude::*;
use serenity::prelude::*;

#[command]
#[description("Prints the bot version")]
async fn version(ctx: &Context, msg: &Message) -> CommandResult {
    let _ = msg
        .channel_id
        .say(&ctx.http, env!("CARGO_PKG_VERSION"))
        .await;
    Ok(())
}

#[group]
#[description("Meta commands")]
#[commands(version)]
struct Meta;
