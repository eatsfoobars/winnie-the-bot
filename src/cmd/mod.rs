use serenity::framework::StandardFramework;

mod admin;
mod meta;
mod roles;
mod utils;

pub fn configure_framework(framework: StandardFramework) -> StandardFramework {
    framework
        .group(&admin::ADMIN_GROUP)
        .group(&meta::META_GROUP)
        .group(&roles::ROLES_GROUP)
        .group(&utils::UTILS_GROUP)
}
