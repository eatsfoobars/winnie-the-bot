use ::zalgo::{Generator, GeneratorArgs, ZalgoSize};
use itertools::Itertools;
use serenity::framework::standard::{
    macros::{command, group},
    Args, CommandResult,
};
use serenity::model::prelude::*;
use serenity::prelude::*;
use std::char::from_u32;

const CLAP: &str = "👏";
const COM_LENNY: &str = "(☭ ͜ʖ ☭)";
const LENNY: &str = "( ͡° ͜ʖ ͡°)";
const SHRUG: &str = "¯\\_(ツ)_/¯";

#[command]
#[description("ＡＥＳＴＨＥＴＩＣ")]
async fn aes(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let full_width = args
        .message()
        .chars()
        .map(|c| {
            let x = c as u32;
            match x {
                0x0020 => '　', // " "
                0x0021..=0x007e => from_u32(x + 0xfee0).unwrap_or(c),
                _ => c,
            }
        })
        .collect::<String>();

    let _ = msg.channel_id.say(&ctx.http, full_width).await;
    Ok(())
}

#[command]
#[description("lenny but commie")]
async fn comlenny(ctx: &Context, msg: &Message) -> CommandResult {
    let _ = msg.channel_id.say(&ctx.http, COM_LENNY).await;
    Ok(())
}

fn clapped(s: &str, sep: &str) -> String {
    s.split(char::is_whitespace).intersperse(sep).collect()
}

#[command]
#[description("👏")]
async fn clap(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let _ = msg
        .channel_id
        .say(&ctx.http, clapped(args.message(), CLAP))
        .await;
    Ok(())
}

#[command]
#[description("👏 but your own thing instead")]
async fn clapwith(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let sep = args.single::<String>().unwrap();
    let content = args.rest();
    let _ = msg.channel_id.say(&ctx.http, clapped(content, &sep)).await;
    Ok(())
}

#[command]
#[description("Coin flip")]
async fn flip(ctx: &Context, msg: &Message) -> CommandResult {
    let _ = msg
        .channel_id
        .say(
            &ctx.http,
            match rand::random::<f64>() {
                x if x < 0.5 => "Heads",
                _ => "Tails",
            },
        )
        .await;
    Ok(())
}

#[command]
#[description("lenny")]
async fn lenny(ctx: &Context, msg: &Message) -> CommandResult {
    let _ = msg.channel_id.say(&ctx.http, LENNY).await;
    Ok(())
}

#[command]
#[description("mOcK")]
async fn mock(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let mocked = args
        .message()
        .chars()
        .enumerate()
        .map(|(i, c)| {
            let lower = c.to_lowercase().next().unwrap();
            let upper = c.to_uppercase().next().unwrap();
            match lower {
                'l' => 'L',
                'i' => 'I',
                _ => {
                    if rand::random::<f64>() < 0.1 {
                        if i % 2 == 0 {
                            upper
                        } else {
                            lower
                        }
                    } else if i % 2 == 0 {
                        lower
                    } else {
                        upper
                    }
                }
            }
        })
        .collect::<String>();

    let _ = msg.channel_id.say(&ctx.http, mocked).await;
    Ok(())
}

#[command]
#[description("Pings the user")]
async fn ping(ctx: &Context, msg: &Message) -> CommandResult {
    let _ = msg.reply(ctx, "Pong!").await;
    Ok(())
}

#[command]
#[description("a e s t h e t i c")]
async fn pooraes(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let _ = msg
        .channel_id
        .say(
            &ctx.http,
            args.message().chars().intersperse(' ').collect::<String>(),
        )
        .await;
    Ok(())
}

#[command]
#[description("*shrugs*")]
#[aliases("shruggie")]
async fn shrug(ctx: &Context, msg: &Message) -> CommandResult {
    let _ = msg.channel_id.say(&ctx.http, SHRUG).await;
    Ok(())
}

#[command]
#[description("He comes")]
async fn zalgo(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let mut zalgod = String::new();
    {
        let mut generator = Generator::new();
        let zalgo_args = GeneratorArgs::new(true, true, true, ZalgoSize::Maxi);
        generator.gen(args.message(), &mut zalgod, &zalgo_args);
    }
    let _ = msg.channel_id.say(&ctx.http, zalgod).await;
    Ok(())
}

#[group]
#[description("Utility commands")]
#[commands(
    aes, comlenny, clap, clapwith, flip, lenny, mock, ping, pooraes, shrug, zalgo
)]
struct Utils;
