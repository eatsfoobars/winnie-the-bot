use crate::{config, utils::get_role_id};
use serenity::framework::standard::{
    macros::{command, group},
    Args, CommandError, CommandResult,
};
use serenity::model::prelude::*;
use serenity::prelude::*;

#[command]
#[min_args(1)]
#[description("Assigns a role to the user")]
async fn iam(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let role_name = args.message();

    let guild_lock = &msg.guild(&ctx.cache).await.unwrap();
    let guild = &guild_lock.read().await;

    let role_id = get_role_id(&guild.roles, role_name)?;

    let mut member = guild.member(ctx, msg.author.id).await?;

    if member
        .user
        .has_role(&ctx, msg.guild_id.unwrap(), role_id)
        .await?
    {
        return Err(CommandError::from("You already have that role"));
    }

    if config::CONFIG
        .guilds()
        .get(&guild.id)
        .map(|c| c.is_protected(role_id))
        .unwrap_or(false)
        && !member
            .permissions(&ctx.cache)
            .await
            .ok()
            .map(|p| p.manage_roles())
            .unwrap_or(true)
    {
        return Err(CommandError::from("Role is protected"));
    }

    member
        .add_role(&ctx.http, role_id)
        .await
        .map_err(|_| CommandError::from("Could not assign role to user"))?;

    msg.channel_id.say(&ctx.http, "Role assigned!").await?;

    Ok(())
}

#[command]
#[min_args(1)]
#[aliases("iamnot")]
#[description("Removes a role from the user")]
async fn iamn(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let role_name = args.message();

    let guild_lock = &msg.guild(&ctx.cache).await.unwrap();
    let guild = &guild_lock.read().await;

    let role_id = get_role_id(&guild.roles, role_name)?;

    let mut member = guild.member(ctx, msg.author.id).await?;

    if !member
        .user
        .has_role(&ctx, msg.guild_id.unwrap(), role_id)
        .await?
    {
        return Err(CommandError::from("You don't have that role"));
    }

    if config::CONFIG
        .guilds()
        .get(&guild.id)
        .map(|c| c.is_protected(role_id))
        .unwrap_or(false)
        && !member
            .permissions(&ctx.cache)
            .await
            .ok()
            .map(|p| p.manage_roles())
            .unwrap_or(true)
    {
        return Err(CommandError::from("Role is protected"));
    }

    member
        .remove_role(&ctx.http, role_id)
        .await
        .map_err(|_| CommandError::from("Could not remove role from user"))?;

    msg.channel_id.say(&ctx.http, "Role removed!").await?;

    Ok(())
}

#[group]
#[description("Role commands")]
#[commands(iam, iamn)]
struct Roles;
