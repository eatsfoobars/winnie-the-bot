use log::error;
use serenity::model::prelude::*;
use serenity::prelude::*;
use serenity::Result as SerenityResult;

const GET_MESSAGE_LIMIT: u64 = 100;

enum GetMessageDirection {
    Before(MessageId),
    After(MessageId),
}

#[allow(dead_code)]
pub enum MessageRange {
    Between(MessageId, MessageId),
    AllBefore(MessageId),
    AllAfter(MessageId),
    Before(MessageId, u64),
    After(MessageId, u64),
}

impl MessageRange {
    fn get_direction(&self) -> GetMessageDirection {
        match self {
            MessageRange::AllBefore(message_id) | MessageRange::Before(message_id, _) => {
                GetMessageDirection::Before(*message_id)
            }
            MessageRange::AllAfter(message_id) | MessageRange::After(message_id, _) => {
                GetMessageDirection::After(*message_id)
            }
            MessageRange::Between(_, message_id) => GetMessageDirection::Before(*message_id),
        }
    }

    fn get_limit(&self) -> Option<u64> {
        match self {
            MessageRange::Before(_, lim) | MessageRange::After(_, lim) => Some(*lim),
            _ => None,
        }
    }
}

async fn get_messages_with_limit(
    ctx: &Context,
    channel_id: ChannelId,
    lim: u64,
    dir: GetMessageDirection,
) -> SerenityResult<Vec<Message>> {
    channel_id
        .messages(&ctx.http, |m| {
            let m = m.limit(lim as u64);
            match dir {
                GetMessageDirection::Before(message_id) => m.before(message_id),
                GetMessageDirection::After(message_id) => m.after(message_id),
            }
        })
        .await
}

pub async fn get_messages(
    ctx: &Context,
    channel_id: ChannelId,
    mut range: MessageRange,
) -> Vec<Message> {
    let mut messages = vec![];
    let goal = range.get_limit();
    loop {
        let limit = if let Some(goal) = goal {
            GET_MESSAGE_LIMIT.min(goal - (messages.len() as u64))
        } else {
            GET_MESSAGE_LIMIT
        };

        if limit == 0 {
            break;
        }

        let message_batch =
            match get_messages_with_limit(&ctx, channel_id, limit, range.get_direction()).await {
                Err(e) => {
                    error!("Error when getting messages: {}", e);
                    break;
                }
                Ok(m) => m,
            };

        let mut filtered_messages = if let MessageRange::Between(after_id, _) = range {
            message_batch
                .into_iter()
                .take_while(|m| m.id != after_id)
                .collect()
        } else {
            message_batch
        };

        let size = filtered_messages.len();
        if size == 0 {
            break;
        }

        range = match range {
            MessageRange::AllBefore(_) => {
                MessageRange::AllBefore(filtered_messages.last().unwrap().id)
            }
            MessageRange::AllAfter(_) => MessageRange::AllAfter(filtered_messages[0].id),
            MessageRange::Before(_, goal) => {
                MessageRange::Before(filtered_messages.last().unwrap().id, goal - (size as u64))
            }
            MessageRange::After(_, goal) => {
                MessageRange::After(filtered_messages[0].id, goal - (size as u64))
            }
            MessageRange::Between(after_id, _) => {
                MessageRange::Between(after_id, filtered_messages.last().unwrap().id)
            }
        };

        messages.append(&mut filtered_messages);

        if size < (GET_MESSAGE_LIMIT as usize) {
            break;
        }
    }

    messages
}
