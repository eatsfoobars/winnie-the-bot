mod ephemeral;
mod join_messages;
mod volcel_police;

use crate::config;

use async_trait::async_trait;
use serenity::client::{Context, EventHandler};
use serenity::model::prelude::*;

pub struct Handler;

#[async_trait]
impl EventHandler for Handler {
    async fn guild_create(&self, ctx: Context, guild: Guild, _: bool) {
        if let Some(cfg) = config::CONFIG
            .guilds()
            .get(&guild.id)
            .and_then(|c| c.ephemeral_config())
        {
            ephemeral::guild_create_handler(&ctx, &guild, cfg.channel, cfg.timeout).await;
        }
    }

    async fn guild_member_addition(&self, ctx: Context, guild_id: GuildId, member: Member) {
        if let Some(cfg) = config::CONFIG
            .guilds()
            .get(&guild_id)
            .and_then(|c| c.join_message_config())
        {
            join_messages::new_join(&ctx, &member, cfg).await;
        }
    }

    async fn message(&self, ctx: Context, msg: Message) {
        if let Some(guild_id) = msg.guild_id {
            if let Some(true) = config::CONFIG
                .guilds()
                .get(&guild_id)
                .map(|c| c.volcel_police())
            {
                volcel_police::new_message_handler(&ctx, &msg).await;
            }

            if let Some(cfg) = config::CONFIG
                .guilds()
                .get(&guild_id)
                .and_then(|c| c.ephemeral_config())
            {
                if cfg.channel == msg.channel_id {
                    ephemeral::new_message_handler(&ctx, &msg, cfg.timeout).await;
                }
            }
        }
    }

    async fn message_delete(&self, ctx: Context, channel_id: ChannelId, message_id: MessageId) {
        if let Some(channel) = ctx.cache.read().await.guild_channel(channel_id) {
            let guild_id = channel.read().await.guild_id;

            if let Some(cfg) = config::CONFIG
                .guilds()
                .get(&guild_id)
                .and_then(|c| c.ephemeral_config())
            {
                if cfg.channel == channel_id {
                    ephemeral::delete_message_handler(&ctx, message_id).await;
                }
            }
        }
    }

    async fn message_delete_bulk(
        &self,
        ctx: Context,
        channel_id: ChannelId,
        message_ids: Vec<MessageId>,
    ) {
        if let Some(channel) = ctx.cache.read().await.guild_channel(channel_id) {
            let guild_id = channel.read().await.guild_id;

            if let Some(cfg) = config::CONFIG
                .guilds()
                .get(&guild_id)
                .and_then(|c| c.ephemeral_config())
            {
                if cfg.channel == channel_id {
                    for message_id in message_ids {
                        ephemeral::delete_message_handler(&ctx, message_id).await;
                    }
                }
            }
        }
    }
}
