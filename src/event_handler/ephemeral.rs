use crate::delete_message_queue::*;
use crate::get_messages::*;
use chrono::prelude::*;

use log::error;
use serenity::model::prelude::*;
use serenity::prelude::*;
use std::time::Duration;

pub async fn guild_create_handler(
    ctx: &Context,
    guild: &Guild,
    channel_id: ChannelId,
    millis: u64,
) {
    if let Some(channel) = guild.channels.get(&channel_id) {
        if let Some(last_message_id) = channel.last_message_id {
            let mut messages =
                get_messages(&ctx, channel_id, MessageRange::AllBefore(last_message_id)).await;
            if let Ok(message) = channel.message(&ctx, last_message_id).await {
                messages.push(message);
            }
            let mut data = ctx.data.write().await;
            let tx = data.get_mut::<DeleteMessageQueue>().unwrap();
            for msg in messages {
                let elapsed = Utc::now()
                    .signed_duration_since(msg.timestamp)
                    .to_std()
                    .unwrap();
                if tx
                    .send(Command::Insert(
                        msg.id,
                        (ctx.http.clone(), msg.channel_id, msg.id),
                        Duration::from_millis(millis)
                            .checked_sub(elapsed)
                            .unwrap_or_default(),
                    ))
                    .is_err()
                {
                    error!("Error when sending insert for {}", msg.id);
                }
            }
        }
    }
}

pub async fn new_message_handler(ctx: &Context, msg: &Message, millis: u64) {
    let mut data = ctx.data.write().await;
    let tx = data.get_mut::<DeleteMessageQueue>().unwrap();

    if tx
        .send(Command::Insert(
            msg.id,
            (ctx.http.clone(), msg.channel_id, msg.id),
            Duration::from_millis(millis),
        ))
        .is_err()
    {
        error!("Error when sending insert for {}", msg.id);
    }
}

pub async fn delete_message_handler(ctx: &Context, message_id: MessageId) {
    let mut data = ctx.data.write().await;
    let tx = data.get_mut::<DeleteMessageQueue>().unwrap();

    if tx.send(Command::Delete(message_id)).is_err() {
        error!("Error when sending delete for {}", message_id);
    }
}
