use crate::config::JoinMessageConfig;
use serenity::client::Context;
use serenity::model::prelude::*;

pub async fn new_join(ctx: &Context, member: &Member, cfg: &JoinMessageConfig) {
    let _ = cfg
        .channel
        .say(
            &ctx.http,
            cfg.message.replace("$USER", &member.user.id.mention()),
        )
        .await;
}
