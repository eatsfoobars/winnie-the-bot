use lazy_static::lazy_static;
use regex::{Regex, RegexBuilder};
use serenity::http::AttachmentType;
use serenity::model::prelude::*;
use serenity::prelude::*;
use std::borrow::Cow;

const MESSAGE: &str = "The VOLCEL POLICE are on the scene!

PLEASE KEEP YOUR VITAL ESSENCES TO YOURSELVES AT ALL TIMES.

تمنحك شرطة Volcel القوة المعنوية للنجاح في هدفك تذكر أن تبقي السوائل الحيوية لنفسك في هذا الشهر الكريم. إذا كنت تستطيع قراءة هذا ، الرجاء المساعدة في الترجمة الصحيحة - القبلات ، التعديل";

const IMAGE: &[u8] = include_bytes!("../resources/volcel_police.jpg");
const FILENAME: &str = "volcel_police.jpg";

pub async fn new_message_handler(ctx: &Context, msg: &Message) {
    lazy_static! {
        static ref RE: Regex = RegexBuilder::new(r"\bvolcel\s+police\b")
            .case_insensitive(true)
            .build()
            .unwrap();
    }

    if msg.author.id != ctx.cache.read().await.user.id && RE.is_match(&msg.content) {
        let _ = msg
            .channel_id
            .send_message(&ctx.http, |m| {
                m.content(MESSAGE);
                m.add_file(AttachmentType::Bytes {
                    data: Cow::Borrowed(IMAGE),
                    filename: FILENAME.to_string(),
                });
                m
            })
            .await;
    }
}
