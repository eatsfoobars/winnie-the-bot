use lazy_static::lazy_static;
use log::warn;
use serde::Deserialize;
use serenity::model::prelude::*;
use std::{
    collections::{HashMap, HashSet},
    fs,
};

const CONFIG_FILENAME: &str = "config.toml";

#[derive(Default, Deserialize)]
pub struct Config {
    #[serde(default)]
    owners: HashSet<UserId>,

    #[serde(default)]
    guilds: HashMap<GuildId, GuildConfig>,
}

#[derive(Deserialize)]
pub struct GuildConfig {
    #[serde(default)]
    protected_roles: HashSet<RoleId>,

    #[serde(default)]
    volcel_police: bool,

    ephemeral: Option<EphemeralConfig>,
    join_message: Option<JoinMessageConfig>,
}

#[derive(Deserialize)]
pub struct JoinMessageConfig {
    pub message: String,
    pub channel: ChannelId,
    pub role: Option<RoleId>,
    pub post_verification_message: Option<String>,
    pub post_verification_channel: Option<ChannelId>,
}

#[derive(Deserialize)]
pub struct EphemeralConfig {
    pub channel: ChannelId,
    pub timeout: u64,
}

impl GuildConfig {
    pub fn is_protected(&self, role_id: RoleId) -> bool {
        self.protected_roles.contains(&role_id)
    }
    pub fn volcel_police(&self) -> bool {
        self.volcel_police
    }
    pub fn ephemeral_config(&self) -> Option<&EphemeralConfig> {
        self.ephemeral.as_ref()
    }
    pub fn join_message_config(&self) -> Option<&JoinMessageConfig> {
        self.join_message.as_ref()
    }
}

impl Config {
    pub fn get_owners(&self) -> &HashSet<UserId> {
        &self.owners
    }
    pub fn guilds(&self) -> &HashMap<GuildId, GuildConfig> {
        &self.guilds
    }
}

lazy_static! {
    pub static ref CONFIG: Config = fs::read_to_string(CONFIG_FILENAME)
        .map_err(|_| warn!("Couldn't read config file, using default config"))
        .and_then(|s| toml::from_str(&*s)
            .map_err(|e| warn!("Couldn't parse config (using default config): {}", e)))
        .unwrap_or_default();
}
