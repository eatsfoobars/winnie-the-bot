//#![feature(async_closure)]

mod cmd;
mod config;
mod delete_message_queue;
mod event_handler;

pub mod get_messages;
pub mod utils;

use futures::prelude::*;
use log::error;
use serenity::framework::standard::macros::{help, hook};
use serenity::framework::standard::{
    help_commands, Args, CommandError, CommandGroup, CommandResult,
    DispatchError::{self, *},
    HelpOptions, StandardFramework,
};
use serenity::model::prelude::*;
use serenity::prelude::*;
use std::collections::HashSet;
use std::env;

pub use delete_message_queue::*;

const DISCORD_TOKEN_ENV: &str = "DISCORD_TOKEN";
const PREFIXES_ENV: &str = "PREFIXES";
const DEFAULT_PREFIXES: &str = ";'";

#[help]
async fn bot_help(
    context: &Context,
    msg: &Message,
    args: Args,
    help_options: &'static HelpOptions,
    groups: &[&'static CommandGroup],
    owners: HashSet<UserId>,
) -> CommandResult {
    help_commands::with_embeds(context, msg, args, &help_options, groups, owners).await
}

#[hook]
async fn after(ctx: &Context, msg: &Message, _cmd: &str, err: CommandResult) {
    if let Err(CommandError(why)) = err {
        if let Err(e) = msg.channel_id.say(&ctx.http, why).await {
            error!("Error when trying to send failure message: {:?}", e);
        }
    }
}

#[hook]
async fn on_dispatch_error(ctx: &Context, msg: &Message, err: DispatchError) {
    match err {
        NotEnoughArguments { min, given } => {
            let s = format!("Need {} arguments, but only got {}", min, given);

            let _ = msg.channel_id.say(&ctx.http, &s).await;
        }
        LackingPermissions(_) => {
            let _ = msg
                .channel_id
                .say(&ctx.http, "Insufficient permissions")
                .await;
        }
        IgnoredBot => {}
        _ => {
            error!("Unknown error occured: {:?}", err);
            let _ = msg.channel_id.say(&ctx.http, "Unknown error").await;
        }
    }
}

#[tokio::main]
async fn main() {
    lazy_static::initialize(&config::CONFIG);
    env_logger::init();

    kankyo::load(true).expect("Failed to load environmental variables");

    let token = env::var(DISCORD_TOKEN_ENV).unwrap_or_else(|_| {
        panic!(
            "Expected Discord token in environmental variable {}",
            DISCORD_TOKEN_ENV
        )
    });

    let prefixes = env::var(PREFIXES_ENV)
        .unwrap_or_else(|_| DEFAULT_PREFIXES.to_string())
        .chars()
        .collect::<Vec<char>>();

    let mut framework = StandardFramework::new()
        .configure(|c| {
            c.prefixes(&prefixes)
                .no_dm_prefix(true)
                .case_insensitivity(true)
                .owners(config::CONFIG.get_owners().clone())
        })
        .help(&BOT_HELP)
        .after(after)
        .on_dispatch_error(on_dispatch_error);

    framework = cmd::configure_framework(framework);

    let mut client = Client::new(&token)
        .event_handler(event_handler::Handler)
        .framework(framework)
        .await
        .expect("Failed to init the Discord client");

    let queue = DeleteMessageQueue::new();

    {
        let mut data = client.data.write().await;
        data.insert::<DeleteMessageQueue>(queue.tx.clone());
    }

    tokio::spawn(
        queue.for_each_concurrent(None, |(http, channel_id, message_id)| async move {
            if let Err(e) = channel_id.delete_message(&http, message_id).await {
                error!("Error when deleting {}: {:?}", message_id, e);
            }
        }),
    );

    client
        .start()
        .await
        .expect("Failed to start the Discord client");
}
