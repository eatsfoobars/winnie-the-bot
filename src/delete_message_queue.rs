use log::debug;
use serenity::http::Http;
use serenity::model::prelude::*;
use serenity::prelude::*;
use std::collections::HashMap;
use std::pin::Pin;
use std::sync::Arc;
use std::task::Poll;
use std::time::Duration;
use tokio::stream::Stream;
use tokio::sync::mpsc::{unbounded_channel, UnboundedReceiver, UnboundedSender};
use tokio::time::delay_queue::{DelayQueue, Key as TimerKey};

type Key = MessageId;
type Item = (Arc<Http>, ChannelId, MessageId);

pub enum Command {
    Insert(Key, Item, Duration),
    Delete(Key),
}

pub struct DeleteMessageQueue {
    pub queue: DelayQueue<Item>,
    map: HashMap<Key, TimerKey>,
    pub tx: UnboundedSender<Command>,
    pub rx: UnboundedReceiver<Command>,
}

impl DeleteMessageQueue {
    pub fn new() -> Self {
        Default::default()
    }

    pub fn insert(&mut self, key: Key, item: Item, duration: Duration) {
        self.map.insert(key, self.queue.insert(item, duration));
    }

    pub fn remove(&mut self, key: Key) {
        if let Some(timer_key) = self.map.remove(&key) {
            self.queue.remove(&timer_key);
        }
    }
}

impl Default for DeleteMessageQueue {
    fn default() -> Self {
        let (tx, rx) = unbounded_channel();
        DeleteMessageQueue {
            queue: DelayQueue::new(),
            map: HashMap::new(),
            tx,
            rx,
        }
    }
}

impl Stream for DeleteMessageQueue {
    type Item = (Arc<Http>, ChannelId, MessageId);

    fn poll_next(
        mut self: Pin<&mut Self>,
        ctx: &mut std::task::Context,
    ) -> Poll<Option<Self::Item>> {
        while let Poll::Ready(Some(t)) = Pin::new(&mut self.rx).poll_next(ctx) {
            match t {
                Command::Insert(key, item, duration) => {
                    debug!("{:?} {:?} {:?}", key, (item.1, item.2), duration);
                    self.insert(key, item, duration);
                }
                Command::Delete(key) => {
                    debug!("{:?}", key);

                    self.remove(key);
                }
            }
        }

        if let Poll::Ready(Some(Ok(exp))) = Pin::new(&mut self.queue).poll_next(ctx) {
            let (http, channel_id, message_id) = exp.into_inner();

            debug!("{:?} {:?}", channel_id, message_id);

            self.map.remove(&message_id);

            return Poll::Ready(Some((http, channel_id, message_id)));
        }

        Poll::Pending
    }
}

impl TypeMapKey for DeleteMessageQueue {
    type Value = UnboundedSender<Command>;
}
