# Changelog

## 1.7.0 (2019-08-21)
* Add verify command

## 1.6.5 (2019-07-25)
* Update Serenity fork
* Completely refactor clear command
* Add ephemeral messages to queue on connect

## 1.6.4 (2019-07-24)
* Switch to my Serenity fork
* Minor fixes

## 1.6.3 (2019-07-23)
* Fix delete message queue implementation
* Use embeds for help command

## 1.6.2 (2019-07-22)
* Owner support
* Better config handling

## 1.6.1 (2019-07-22)
* Hide commands from help when user is lacking permissions
* Minor ephemeral fix

## 1.6.0 (2019-07-20)
* Add ephemeral module

## 1.5.0 (2019-07-17)
* Add `join_message` module
* Allow configuration and disabling of modules from the config file

## 1.4.0 (2019-07-17)
* Update `serenity` to 0.6.2

## 1.3.0 (2019-04-12)
* Add `setrole`, `unsetrole` commands
* Add protected roles

## 1.2.1 (2019-04-11)
* Fix full width space in `aes`

## 1.2.0 (2019-04-06)
* Support multiple prefixes

## 1.1.0 (2019-03-04)
* Ignore case when searching for role

## 1.0.0 (2019-02-28)
* Implement `zalgo` command

## 0.9.0 (2019-02-28)
* Implement flip, clap, lenny, shrug commands

## 0.8.0 (2019-02-27)
* Implement `aes`, `pooraes`, `mock` commands

## 0.7.0 (2019-02-26)
* Implement `kick`, `ban` commands
* Log unknown errors to the console

## 0.6.1 (2019-02-26)
* Fix clearing range

## 0.6.0 (2019-02-26)
* Implement clearing range

## 0.5.0 (2019-02-25)
* Add `volcel police` module
* Use `lazy_static` for the 2 weeks duration
* Make `clear` safer to use

## 0.4.0 (2019-02-25)
* Add `clear inf` command
* Use whole message as argument instead of getting the first quoted argument

## 0.3.1 (2019-02-22)

* Use upstream `serenity` package (`Content-Length` fix got merged).

## 0.3.0 (2019-02-21)

* Add `clear` command that clears `n` messages from the current channel.

## 0.2.0 (2019-02-17)

* Add `version` command that prints the version of the bot.
* Inform the user if they already have a role, or if they don't, when calling `iam` and `iamn` respectively.

## 0.1.0 (2019-02-17)

* Initial release.
* Add `ping` command that pings the user.
* Add `iam` and `iamn` commands that assign and remove a role from the user.
* Add `mkrole` and `rmrole` commands that create or remove a role from the server, assuming the caller has got the appropriate permissions.
* Add `channame` command that sets the name of the current channel, assuming the caller has got the appropriate permissions.
